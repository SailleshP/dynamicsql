﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DynamicTableName.aspx.cs" Inherits="DynamicSqlQuery.DynamicTableName" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Employee Search</title>
    <link rel="stylesheet"
        href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
        type="text/css" />
</head>
<body style="padding-top: 10px">
    <div class="col-xs-8 col-xs-offset-2">
        <form id="form1" runat="server" class="form-horizontal">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3>Table Lookup</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="inputTableName" class="control-label col-xs-4">
                            Table Name
                        </label>
                        <div class="col-xs-8">
                            <input type="text" runat="server" class="form-control"
                                id="inputTableName" placeholder="Please enter table name" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-10 col-xs-offset-2">
                            <asp:Button ID="btnLoadData" runat="server" Text="Load Data"
                                CssClass="btn btn-primary" OnClick="btnLoadData_Click" />
                            <asp:Label ID="lblError" runat="server" CssClass="text-danger">
                            </asp:Label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3>Table Data</h3>
                </div>
                <div class="panel-body">
                    <div class="col-xs-10">
                        <asp:GridView CssClass="table table-bordered"
                            ID="gvTableData" runat="server">
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </form>
    </div>
</body>
</html>