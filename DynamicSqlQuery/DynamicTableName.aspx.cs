﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DynamicSqlQuery
{
    public partial class DynamicTableName : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLoadData_Click(object sender, EventArgs e)
        {
            try
            {
                if (inputTableName.Value.Trim() != "")
                {
                    string strConnection = ConfigurationManager.ConnectionStrings["db"].ConnectionString;

                    using (SqlConnection con = new SqlConnection(strConnection))
                    {
                        SqlCommand cmd = new SqlCommand();
                        cmd.Connection = con;
                        cmd.CommandText = "sp_dynamicTableName";
                        cmd.CommandType = CommandType.StoredProcedure;

                        SqlParameter param = new
                            SqlParameter("@TableName", inputTableName.Value);
                        param.SqlDbType = SqlDbType.NVarChar;
                        param.Size = 100;
                        cmd.Parameters.Add(param);

                        con.Open();
                        SqlDataReader rdr = cmd.ExecuteReader();
                        gvTableData.DataSource = rdr;
                        gvTableData.DataBind();
                    }
                }
                lblError.Text = "";
            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
            }
        }
    }
}